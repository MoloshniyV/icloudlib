//
//  main.m
//  NIXICloudPresentation
//
//  Created by Admin on 4/10/14.
//  Copyright (c) 2014 NIX. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NIXAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NIXAppDelegate class]));
    }
}
