//
//  NIXViewController.m
//  NIXICloudPresentation
//
//  Created by Admin on 4/10/14.
//  Copyright (c) 2014 NIX. All rights reserved.
//

#import "NIXViewController.h"
#import "NIXDocument.h"

#define DOCUMENTS @"Documents"

#define LOCAL_DOCUMENT_DIRECTORY [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]

#define ICLOUD_DOCUMENTS_DIRECTORY [[_fileManager URLForUbiquityContainerIdentifier:nil] URLByAppendingPathComponent:DOCUMENTS]

@interface NIXViewController ()

@property(strong, nonatomic)NSMetadataQuery *query;
@property(strong, nonatomic)NSFileManager *fileManager;

@end

@implementation NIXViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
    _fileManager = [NSFileManager defaultManager];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeQuery];
}

- (void)initializeQuery
{
    _query =  [[NSMetadataQuery alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K LIKE '*'", NSMetadataItemFSNameKey];
    NSArray *searchScoope = [NSArray arrayWithObject:NSMetadataQueryUbiquitousDocumentsScope];
    
    [_query setSearchScopes:searchScoope];
    [_query setPredicate:predicate];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iCloudDidUpdated) name:NSMetadataQueryDidUpdateNotification object:nil];
    [_query startQuery];
}

- (IBAction)removeFileFromiCloud:(id)sender
{
  
}

- (IBAction)addFileToiCloud:(id)sender
{

}

- (void)iCloudDidUpdated
{
}

@end
