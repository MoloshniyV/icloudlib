//
//  NIXDocument.m
//  NIXICloudPresentation
//
//  Created by Admin on 4/10/14.
//  Copyright (c) 2014 NIX. All rights reserved.
//

#import "NIXDocument.h"

@implementation NIXDocument

- (BOOL)loadFromContents:(id)contents ofType:(NSString *)typeName error:(NSError **)outError
{
    [self setData:(NSData *)contents];
    return YES;
}

- (id)contentsForType:(NSString *)typeName error:(NSError **)outError
{
    return _data;
}

@end
// Абсолютно не примечательный класс Билл