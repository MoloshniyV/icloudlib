//
//  NIXAppDelegate.h
//  NIXICloudPresentation
//
//  Created by Admin on 4/10/14.
//  Copyright (c) 2014 NIX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NIXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
