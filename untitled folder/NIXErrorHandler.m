//
//  NIXErrorHandler.m
//  NIXICloudPresentation
//
//  Created by Admin on 4/11/14.
//  Copyright (c) 2014 NIX. All rights reserved.
//

#import "NIXErrorHandler.h"

#define ICLOUD_ERROR_DOMAIN  @"iCloudHelperErrorDomain"

@interface NIXErrorHandler()
@property(strong, nonatomic)NSFileManager *fileManager;
@end

@implementation NIXErrorHandler

- (id)init
{
    self = [super init];
    
    if (self != nil)
    {
        _fileManager = [NSFileManager defaultManager];
    }
    
    return self;
}

- (NSError *)checkErrorInCopyFileFrom:(NSString *)fromPath to:(NSString *)toPath
{
    BOOL isDirectory;
    
    [_fileManager fileExistsAtPath:toPath isDirectory:&isDirectory];
    
    if (isDirectory)
    {
        return [self generateErrorWithCode:HelperErrorCodeNoFileExtensionFound];
    }

    BOOL directoryExist = [_fileManager fileExistsAtPath:[toPath stringByDeletingLastPathComponent]];

    if (!directoryExist)
    {
        return [self generateErrorWithCode:HelperErrorCodeNoDirectoryExist];
    }
    
    return nil;
}

- (NSError *)checkErrorInCopyFromLocalToiCloudSaveOperation
{
    return [self generateErrorWithCode:HelperErrorCodeCopyToiCloudSaveOperation];
}

- (NSError *)checkErrorInCopyFromLocalToiCloudCloseOperation
{
    return [self generateErrorWithCode:HelperErrorCodeCopyToiCloudCloseOperation];
}










- (NSError *)generateErrorWithCode:(HelperErrorCode)errorCode
{
    NSError *error = nil;
   
    NSDictionary* details = [NSDictionary dictionary];
   
    switch (errorCode)
    {
        case HelperErrorCodeNoFileExtensionFound:
            [details setValue:@"You should specify FILENAME in Path" forKey:NSLocalizedDescriptionKey];
            error = [NSError errorWithDomain:ICLOUD_ERROR_DOMAIN code:HelperErrorCodeNoFileExtensionFound userInfo:details];
            break;
        
        case HelperErrorCodeNoDirectoryExist:
            
            [details setValue:@"Directory path is not exist. You should create directory before copy" forKey:NSLocalizedDescriptionKey];
            error = [NSError errorWithDomain:ICLOUD_ERROR_DOMAIN code:HelperErrorCodeNoDirectoryExist userInfo:details];
            break;
        
        case HelperErrorCodeCopyToiCloudSaveOperation:
            [details setValue:@"Unknown Error when trying to save to iCloud" forKey:NSLocalizedDescriptionKey];
            error = [NSError errorWithDomain:ICLOUD_ERROR_DOMAIN code:HelperErrorCodeNoDirectoryExist userInfo:details];
        
        case HelperErrorCodeCopyToiCloudCloseOperation:
            [details setValue:@"Unknown Error when trying to close saved file" forKey:NSLocalizedDescriptionKey];
            error = [NSError errorWithDomain:ICLOUD_ERROR_DOMAIN code:HelperErrorCodeNoDirectoryExist userInfo:details];
    }
    
    return error;
}
















@end





















