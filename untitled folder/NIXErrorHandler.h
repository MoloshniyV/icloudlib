//
//  NIXErrorHandler.h
//  NIXICloudPresentation
//
//  Created by Admin on 4/11/14.
//  Copyright (c) 2014 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NIXErrorHandler : NSObject

typedef  enum
{
    HelperErrorCodeNoFileExtensionFound = 1,
    HelperErrorCodeNoDirectoryExist,
    HelperErrorCodeCopyToiCloudSaveOperation,
    HelperErrorCodeCopyToiCloudCloseOperation
}HelperErrorCode;

- (NSError *)checkErrorInCopyFileFrom:(NSString *)fromPath to:(NSString *)toPath;
- (NSError *)checkErrorInCopyFromLocalToiCloudSaveOperation;
- (NSError *)checkErrorInCopyFromLocalToiCloudCloseOperation;

@end
