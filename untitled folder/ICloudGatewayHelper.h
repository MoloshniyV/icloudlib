
//
//  ICloudGatewayHelper.h
//  uFlowers
//
//  Created by Admin on 1/15/14.
//
//

#import <Foundation/Foundation.h>

@protocol ICloudHelperProtocol;

@interface ICloudGatewayHelper : NSObject

@property(assign, nonatomic) id<ICloudHelperProtocol> delegate;

+ (id)sharedInstance;

- (BOOL)removeDirectoryContent:(NSString *)directoryPath  error:(NSError **)error; // +

- (BOOL)copyFileFrom:(NSString *)fromPath to:(NSString *)toPath completionSuccess:(void (^)(void))completion completionError:(void (^)(NSError *error))completionError;

- (BOOL)writeStringToFile:(NSString *)filePath  string:(NSString *)string;

- (BOOL)removeFile:(NSString *)filePath error:(NSError **)error;

- (BOOL)copyDirectory:(NSString *)fromPath toDirectory:(NSString *)toPath error:(NSError **)error;

- (BOOL)inializeiCloudWithSearchScoopes:(NSArray *)searchScooopes withPredicat:(NSPredicate *)predicate;

- (void)stopUpdateNotification;

- (NSArray *)getMainArray;



// - (BOOL)moveFileFrom:(NSString *)path to:(NSString *)path error:(NSError **)error;
//
// - (BOOL)moveDirectory:(NSString *)path toDirectory:(NSString*)path error:(NSError **)error;
//
//

@end

@protocol ICloudHelperProtocol<NSObject>

- (void)iCloudUpdated:(NSArray *)queryArray;
- (void)iCloudRecivedData;
@end