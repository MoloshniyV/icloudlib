//
//  ICloudGatewayHelper.m
//  uFlowers
//
//  Created by Admin on 1/15/14.
//
//

#import "ICloudGatewayHelper.h"
#import "NIXErrorHandler.h"
#import "NIXDocument.h"

#define DOCUMENTS @"Documents"

#define ICLOUD_DOCUMENT_DIRECTORY [[_fileManager URLForUbiquityContainerIdentifier:nil] URLByAppendingPathComponent:DOCUMENTS]

#define LOCAL_DOCUMENT_DIRECTORY [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]

@interface ICloudGatewayHelper ()

@property(nonatomic, strong) NIXErrorHandler *nixErrorHandler;

@property(nonatomic, strong) NSFileManager *fileManager;

@property(strong, nonatomic) NSMetadataQuery *query;

@property(strong, nonatomic) NSDate *mainDate;

@property(strong, nonatomic) NSMutableArray *mainQueryArray;

@property(strong, nonatomic) NSTimer *timer;

- (void)copyFromiCloudToLocal:(NSString *)iCloudFilePath localPath:(NSString *)localPath completionBlock:(void (^)(void))completion completionError:(void (^)(NSError *error))completionError;

@end

@implementation ICloudGatewayHelper

@synthesize timer;

+ (id)sharedInstance
{
    static dispatch_once_t once;
    static ICloudGatewayHelper *sharedInstance;
    dispatch_once(&once, ^
                  {
                      sharedInstance = [[self alloc] init];
                  });
    
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    
    if (self != nil)
    {
        [self setFileManager:[NSFileManager defaultManager]];
        [self setMainQueryArray:[NSMutableArray array]];
        _nixErrorHandler = [[NIXErrorHandler alloc] init];
    }
    
    return self;
}

#pragma setupQueryTimer

- (void)updateQuery
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K LIKE '*'", NSMetadataItemFSNameKey];
    NSArray *searchScoope = [NSArray arrayWithObject:NSMetadataQueryUbiquitousDocumentsScope];
    [self inializeiCloudWithSearchScoopes:searchScoope withPredicat:predicate];
  
}

#pragma mark removeLocalFiles

- (BOOL)removeDirectoryContent:(NSString *)directoryPath  error:(NSError **)error
{
    BOOL success = YES;

    NSArray *iCloudDirectoryContent = [_fileManager contentsOfDirectoryAtPath:directoryPath error:error];
    
    for (NSString *fileName in iCloudDirectoryContent)
    {
        success =   [_fileManager removeItemAtPath:[directoryPath stringByAppendingPathComponent:fileName] error:nil];
        
        if (!success)
        {
            NSLog(@"(++++)Failed To remove File");
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)copyFileFrom:(NSString *)fromPath to:(NSString *)toPath completionSuccess:(void (^)(void))completionSuccess completionError:(void (^)(NSError *error))completionError
{
    NSError *error = [_nixErrorHandler checkErrorInCopyFileFrom:fromPath to:toPath];
    
    if (error != nil)
    {
        completionError(error);
        return NO;
    }
    
    NSURL *sandBoxURL = [_fileManager URLForUbiquityContainerIdentifier:nil];
    
    if ([toPath rangeOfString:[sandBoxURL path]].location != NSNotFound)
    {
        [self copyFromLocalToiCloud:fromPath iCloudPath:toPath completionSuccess:completionSuccess completionError:completionError];
    }
    else
    {
        [self copyFromiCloudToLocal:fromPath localPath:toPath completionBlock:completionSuccess completionError:completionError];
    }
    
    return YES;
}

- (void)copyFromLocalToiCloud:(NSString *)fromPath iCloudPath:(NSString *)toPath completionSuccess:(void (^)(void))completion completionError:(void (^)(NSError *error))completionError
{
//    if ([_fileManager fileExistsAtPath:toPath])
//    {
//        [self removeFile:toPath error:nil];
//    }
    
    NSURL *toURL = [NSURL fileURLWithPath:toPath];
    NSURL *fromURL = [NSURL fileURLWithPath:fromPath];
    
    NIXDocument *_dbDoc = [[NIXDocument alloc] initWithFileURL:toURL];
    
    _dbDoc.data = [NSData dataWithContentsOfURL:fromURL];
    
    [_dbDoc saveToURL:toURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL saveSuccess)
    {
        if (!saveSuccess)
        {
            NSError *error = [_nixErrorHandler checkErrorInCopyFromLocalToiCloudSaveOperation];
            completionError (error);
        }
         
        [_dbDoc closeWithCompletionHandler:^(BOOL closeSuccess)
        {
            if (!closeSuccess)
            {
                NSError *error = [_nixErrorHandler checkErrorInCopyFromLocalToiCloudCloseOperation];
                completionError (error);
            }
            
            completion();
        }];
    }];
}

- (BOOL)writeStringToFile:(NSString *)filePath  string:(NSString *)string
{
    if ([_fileManager fileExistsAtPath:filePath])
    {
        [self removeFile:filePath error:nil];
    }
    
    NSURL *fileURL = [NSURL fileURLWithPath:filePath];
   
    NIXDocument *doc = [[NIXDocument alloc] initWithFileURL:fileURL];
    
    doc.data = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    if (doc.data == nil)
    {
        return NO;
    }
    
    [doc saveToURL:fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL openSuccess)
    {
        if (!openSuccess)
        {
            NSLog(@"(+++++) DB Write to iCloud NOT SUCCESS");
        }
         
        [doc closeWithCompletionHandler:^(BOOL closeSuccess)
        {
            if (!closeSuccess)
            {
                NSLog(@"(++++)CLOSE DOCUMENT NOT SUCCESS");
            }
        }];
    }];
        
    return YES;
}

- (BOOL)removeFile:(NSString *)filePath error:(NSError **)error
{
    BOOL success = [_fileManager removeItemAtPath:filePath error:error];

    return success;
}

- (BOOL)copyDirectory:(NSString *)fromPath toDirectory:(NSString *)toPath error:(NSError **)error
{
    BOOL directoryExist = [_fileManager fileExistsAtPath:toPath];
       
    if (!directoryExist)
    {
        BOOL success = [_fileManager createDirectoryAtPath:toPath withIntermediateDirectories:NO attributes:nil error:nil];
        
        if (!success)
        {
            NSLog(@"ERROR CANT CREATE DIRECTORY ");
            return NO;
        }
    }

    NSArray *fromFilesList = [_fileManager contentsOfDirectoryAtPath:fromPath error:nil];
    
    for (NSString *fileName in fromFilesList)
    {
        NSString *fromFileName = [fromPath stringByAppendingPathComponent:fileName];
        NSString *toFileName = [toPath stringByAppendingPathComponent:fileName];
        [self copyFileFrom:fromFileName to:toFileName completionSuccess:nil completionError:nil];
    }
    
    return YES;
}



- (void)copyFromiCloudToLocal:(NSString *)iCloudFilePath localPath:(NSString *)localPath completionBlock:(void (^)(void))completion completionError:(void (^)(NSError *error))completionError
{
    [self createDirectoryIfNeededWithPath:localPath];
   
    if ([_fileManager fileExistsAtPath:localPath isDirectory:nil])
    {
        [_fileManager removeItemAtPath:localPath error:nil];
    }
    
//    Reachability *reach = [Reachability reachabilityForInternetConnection];
//    
//    if (![reach isReachable])
//    {
//        completionError();
//        return;
//    }
    
    __block NSError *error = nil;

    NIXDocument *_doc = [[NIXDocument alloc] initWithFileURL:[NSURL fileURLWithPath:iCloudFilePath]];
    
    if (_doc.documentState == UIDocumentStateInConflict)
    {
        [NSFileVersion removeOtherVersionsOfItemAtURL:_doc.fileURL error:nil];
        
        NSArray *conflictVersions = [NSFileVersion unresolvedConflictVersionsOfItemAtURL:_doc.fileURL];
        
        for (NSFileVersion *fileVersion in conflictVersions)
        {
            fileVersion.resolved = YES;
        }
    }
 
    NSURL *iCloudFileURL = [NSURL fileURLWithPath:iCloudFilePath];
   
    NSURL *localFileURL = [NSURL fileURLWithPath:localPath];
    
    NSFileCoordinator *cordinator = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
   
    double delayInSeconds = 1.0;
   
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void)
    {
        [_doc openWithCompletionHandler:^(BOOL success)
        {
            if (!success)
            {
                completionError(nil);
            }
            else
            {
                [cordinator coordinateReadingItemAtURL:iCloudFileURL options:NSFileCoordinatorReadingWithoutChanges writingItemAtURL:localFileURL options:NSFileCoordinatorWritingForReplacing error:&error byAccessor:^(NSURL *newReadingURL, NSURL *newWritingURL)
                {
                    BOOL writeComplite = [_doc.data writeToFile:[newWritingURL path] options:NSDataWritingFileProtectionNone error:&error];
                      
                    if (!writeComplite)
                    {
                        NSLog(@"(+++++)Write DB From iCloud to Local  Success (1) = %d", writeComplite);
                        completionError(nil);
                      
                        return;
                    }
              
                    [_doc closeWithCompletionHandler:^(BOOL success)
                    {
                        if (!success)
                        {
                            NSLog(@"+++ CLOSE DOCUMENTS NOT SUCCESS");
                            completionError(nil);
                            return;
                        }
                        
                        completion();
                    }];
                }];
            }
        }];
    });
}

- (void)createDirectoryIfNeededWithPath:(NSString *)path
{
    NSString *string = [path stringByDeletingLastPathComponent];

    if (![_fileManager fileExistsAtPath:string])
    {
        BOOL success = [_fileManager createDirectoryAtPath:string withIntermediateDirectories:NO attributes:nil error:nil];

        if (!success)
        {
            NSLog(@"ERROR TO createCopyDirectoriesIfNeed");
        }
    }
}

- (void)iCloudUpdatedSuccess:(NSNotification *)notification
{
    [_delegate iCloudRecivedData];
    [self setMainDate:[NSDate date]];
    
    for (NSMetadataItem *item in [_query results])
    {
        NSString *path = [item  valueForAttribute:NSMetadataItemPathKey];
    
        if (![_mainQueryArray containsObject:path])
        {
            [_mainQueryArray addObject:path];
            NSLog(@"item = %@", path);
        }
    }

    if (timer != nil)
    {
        [timer setFireDate:[NSDate dateWithTimeIntervalSinceNow:7]];
    }
    else
    {
        timer = [[NSTimer alloc] initWithFireDate:[NSDate dateWithTimeIntervalSinceNow:7] interval:0 target:self selector:@selector(gogo:) userInfo:nil repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    }
}

- (void)gogo:(NSTimer *)timer1
{
    [timer invalidate];
    timer = nil;
   
    [self removeUnUploadedfiles];
    
    [_delegate iCloudUpdated:[_mainQueryArray copy]];
  
    [_mainQueryArray removeAllObjects];
}

- (void)removeUnUploadedfiles
{
    for (int i = 0; i < [_query resultCount]; i++)
    {
        NSMetadataItem *item = [_query results][i];
        
        if (![[item valueForAttribute:NSMetadataUbiquitousItemIsUploadedKey] boolValue])
        {
            NSString *path = [item valueForAttribute:NSMetadataItemPathKey];
            [_mainQueryArray removeObject:path];
        }
    }
}

- (BOOL)inializeiCloudWithSearchScoopes:(NSArray *)searchScooopes withPredicat:(NSPredicate *)predicate
{
    _query = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSMetadataQueryDidUpdateNotification object:nil];

    [self setQuery:[[NSMetadataQuery alloc] init]];
    [_query setSearchScopes:searchScooopes];
    [_query setNotificationBatchingInterval:1];
  //  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K LIKE '*'", NSMetadataItemFSNameKey];
    
    [_query setPredicate:predicate];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iCloudUpdatedSuccess:) name:NSMetadataQueryDidUpdateNotification object:nil];
   
    [_query performSelectorOnMainThread:@selector(startQuery) withObject:nil waitUntilDone:YES];
 
    return YES;
}

- (void)stopUpdateNotification;
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSMetadataQueryDidUpdateNotification object:nil];
    [_query disableUpdates];
    [_query stopQuery];
}

- (NSArray *)getMainArray
{
    NSMutableArray *resultArray = [NSMutableArray array];
   
    NSArray *directoiesList = [_fileManager contentsOfDirectoryAtPath:[ICLOUD_DOCUMENT_DIRECTORY path] error:nil];
    
    for (NSString *directoryName in directoiesList)
    {
        BOOL isDirectory;
        [_fileManager fileExistsAtPath:[[ICLOUD_DOCUMENT_DIRECTORY URLByAppendingPathComponent:directoryName] path] isDirectory:&isDirectory];
        
        if (isDirectory)
        {
            NSArray *array = [_fileManager contentsOfDirectoryAtPath:[[ICLOUD_DOCUMENT_DIRECTORY URLByAppendingPathComponent:directoryName] path] error:nil];
       
            for (NSString *fileName in array)
            {
                [resultArray addObject:[[[ICLOUD_DOCUMENT_DIRECTORY URLByAppendingPathComponent:directoryName] URLByAppendingPathComponent:fileName] path]];
            }
        }
    }
    
    return [resultArray copy];
}

@end
